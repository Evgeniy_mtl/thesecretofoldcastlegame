package gameObjects;

public class StaticObject implements InteractableObject {

    private String name;
    private KeyItem keyItem;
    private Boolean isActivated = false;

    public StaticObject(String name) {
        this.name = name;
    }

    //If object is activatable defines key item to activate it
    public StaticObject(String name, KeyItem keyItem) {
        this.name = name;
        this.keyItem = keyItem;
    }

    //If player activate object it become activated (messages to player depends on it)
    public Boolean getActivated() {
        return isActivated;
    }

    public void setActivated(Boolean activated) {
        isActivated = activated;
    }

    @Override
    public String toString() {
        return name;
    }
    @Override
    public String getName() {
        return name;
    }

    @Override
    public KeyItem getKey() {
        return keyItem;
    }
}
