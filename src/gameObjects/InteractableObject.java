package gameObjects;

public interface InteractableObject {

    String getName();

    KeyItem getKey();


}
