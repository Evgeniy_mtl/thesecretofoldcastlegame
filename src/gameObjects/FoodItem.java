package gameObjects;

public class FoodItem implements CollectableItem {

    private String name;
    private int restore;

    public FoodItem(String name, int restore) {
        this.name = name;
        this.restore = restore;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

    public int getRestore() {
        return restore;
    }

}
