package gameObjects;

public class KeyItem implements CollectableItem {

    String name;

    public KeyItem(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}
