package gameObjects;


import java.util.HashMap;

public class ContainerObject implements InteractableObject {


    private String name;
    private HashMap<String, CollectableItem> container = new HashMap<>();
    private KeyItem key;


    public ContainerObject(String name) {
        this.name = name;
    }

    public ContainerObject(String name, KeyItem key) {
        this.name = name;
        this.key = key;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public KeyItem getKey() {
        return key;
    }

    public HashMap<String, CollectableItem> getContainer() {
        return container;
    }

    @Override
    public String getName() {
        return name;
    }


}
