package gameObjects;

public interface CollectableItem {

    String getName();
}
