package gameObjects;

import game.GameMessage;
import game.Player;

import java.util.Scanner;

public class RiddleObject implements InteractableObject {

    private String name;
    private String answer;
    private GameMessage message = new GameMessage();


    public RiddleObject(String name, String answer) {
        this.name = name;
        this.answer = answer;
    }

    //Runs player interacting activity, if player input right string gives useful information, if not damages him
    public void run(Player player){
        message.callMessage("./res/" + this.name + ".txt");
        Scanner scanner = new Scanner(System. in);
        String inputString = scanner. nextLine();
        if (inputString.equalsIgnoreCase(answer)){
            message.callMessage("./res/" + this.name + "good.txt");
            if (answer.equalsIgnoreCase("hrad")){
                player.setSuccess(true);
            }
        } else if (inputString.equalsIgnoreCase("zpet")){
            player.setInteractingWithObject(null);
            message.callMessage("./res/" + player.getCurrentLocation().getName() + ".txt");
            return;
        } else{
            message.callMessage("./res/" + this.name + "bad.txt");
            player.setHealth(player.getHealth()-30);
            System.out.print("Cítiš se na " + player.getHealth() + "% \n");
        }
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public KeyItem getKey(){return null;}

}
