package game;

import gameObjects.InteractableObject;
import gameObjects.KeyItem;

import java.util.HashMap;
import java.util.Objects;

import static java.util.Objects.hash;


public class Location  {

    private String name;
    private HashMap <String, InteractableObject> interactableObjects = new HashMap<>();
    private HashMap <String, Location> reachableLocations = new HashMap<>();
    private KeyItem key = null;


    public Location(String name) {
        this.name = name;
    }

    //Constructor if location will be locked
    public Location(String name, KeyItem key) {
        this.name = name;
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public KeyItem getKey() {
        return key;
    }

    public HashMap<String, InteractableObject> getInteractableObjects() {
        return interactableObjects;
    }

    public HashMap<String, Location> getReachableLocations() {
        return reachableLocations;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Location location = (Location) o;
        return Objects.equals(name, location.name);
    }

    @Override
    public int hashCode() {

        return hash(name);
    }
}
