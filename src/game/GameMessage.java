package game;

import java.io.*;


public class GameMessage {

    //Calls message depends on file name
    public void callMessage(String fileName){
        try {
            File file = new File(fileName);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line;
            while((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
            }
        } catch (FileNotFoundException e ){
            System.out.println("Soubor nenalezen\n");
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}
