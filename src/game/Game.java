package game;

import gameObjects.*;

import java.util.HashMap;

public class Game {

    private CommandHandler commandHandler = new CommandHandler(this);
    private GameMessage message = new GameMessage();
    Player player = new Player();
    HashMap<String, Location> locations = new HashMap<>();

    //Starts game
    public void startGame(){
        message.callMessage("./res/start.txt");
        //Main game loop, handles players input until game is end;
        while (!player.isSuccess() && player.getHealth() > 0) {
            commandHandler.handle();
        }
        if (player.isSuccess()){
            System.out.print("Gratuluji, vyhral jsi!");
        } else {
            System.out.print("Hra je ukončena, nezvladl jsi to.");
        }
    }

    //Setting up all game objects and items
    public void initGame(){


        StaticObject police = new StaticObject("police");
        ContainerObject koberec = new ContainerObject("koberec");
        KeyItem klic = new KeyItem("klic");
        koberec.getContainer().put(klic.getName(),klic);

        KeyItem vidlice = new KeyItem("vidlice");
        StaticObject stul = new StaticObject("stul", vidlice);

        KeyItem rukojet = new KeyItem("rukojet");
        KeyItem kresadlo = new KeyItem("kresadlo");
        FoodItem mrkev = new FoodItem("mrkev", 30);
        ContainerObject truhla = new ContainerObject("truhla");
        truhla.getContainer().put(rukojet.getName(),rukojet);
        truhla.getContainer().put(kresadlo.getName(),kresadlo);
        truhla.getContainer().put(mrkev.getName(),mrkev);
        StaticObject krb = new StaticObject("krb",kresadlo);

        FoodItem jablko = new FoodItem("jablko", 50);
        ContainerObject skrin = new ContainerObject("skrin",rukojet);
        skrin.getContainer().put(jablko.getName(),jablko);
        skrin.getContainer().put(vidlice.getName(),vidlice);

        ContainerObject komod = new ContainerObject("komod");
        KeyItem hadr = new KeyItem("hadr");
        komod.getContainer().put(hadr.getName(),hadr);

        StaticObject zrcadlo = new StaticObject("zrcadlo",hadr);

        RiddleObject kniha = new RiddleObject("kniha","Paracels");
        RiddleObject tabulka = new RiddleObject("tabulka", "m");
        RiddleObject zed = new RiddleObject("zed","hrad");

        Location sal = new Location("sal");
        Location koupelna = new Location("koupelna");
        Location jidelna = new Location("jidelna");
        Location kuchyne = new Location("kuchyne");
        Location loznice = new Location("loznice");
        Location vez = new Location("vez");
        Location komora = new Location("komora");
        Location knihovna = new Location("knihovna");
        Location sklep = new Location("sklep",klic);

        sal.getReachableLocations().put(koupelna.getName(),koupelna);
        sal.getReachableLocations().put(jidelna.getName(),jidelna);
        sal.getReachableLocations().put(loznice.getName(),loznice);
        sal.getReachableLocations().put(komora.getName(),komora);
        sal.getReachableLocations().put(knihovna.getName(),knihovna);

        koupelna.getReachableLocations().put(sal.getName(),sal);
        koupelna.getInteractableObjects().put(zrcadlo.getName(),zrcadlo);

        jidelna.getReachableLocations().put(sal.getName(),sal);
        jidelna.getReachableLocations().put(kuchyne.getName(),kuchyne);
        jidelna.getInteractableObjects().put(stul.getName(),stul);
        jidelna.getInteractableObjects().put(krb.getName(),krb);

        kuchyne.getReachableLocations().put(jidelna.getName(),jidelna);
        kuchyne.getInteractableObjects().put(skrin.getName(),skrin);

        loznice.getReachableLocations().put(sal.getName(),sal);
        loznice.getInteractableObjects().put(komod.getName(),komod);
        loznice.getInteractableObjects().put(zed.getName(),zed);

        vez.getReachableLocations().put(komora.getName(),komora);
        vez.getInteractableObjects().put(kniha.getName(),kniha);

        komora.getReachableLocations().put(sal.getName(),sal);
        komora.getReachableLocations().put(vez.getName(),vez);
        komora.getReachableLocations().put(sklep.getName(),sklep);
        komora.getInteractableObjects().put(truhla.getName(),truhla);

        knihovna.getReachableLocations().put(sal.getName(),sal);
        knihovna.getInteractableObjects().put(police.getName(),police);
        knihovna.getInteractableObjects().put(koberec.getName(), koberec);

        sklep.getReachableLocations().put(komora.getName(),komora);
        sklep.getInteractableObjects().put(tabulka.getName(), tabulka);

        locations.put(sal.getName(),sal);
        locations.put(koupelna.getName(),koupelna);
        locations.put(jidelna.getName(),jidelna);
        locations.put(kuchyne.getName(),kuchyne);
        locations.put(loznice.getName(),loznice);
        locations.put(vez.getName(),vez);
        locations.put(komora.getName(),komora);
        locations.put(knihovna.getName(),knihovna);
        locations.put(sklep.getName(),sklep);

        player.setCurrentLocation(sal);
    }



}
