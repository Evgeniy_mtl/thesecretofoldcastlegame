package game;

import gameObjects.ContainerObject;
import gameObjects.RiddleObject;
import gameObjects.StaticObject;

import java.util.Iterator;
import java.util.Scanner;
import java.util.StringTokenizer;

public class CommandHandler {

    private Scanner in = new Scanner(System.in);
    private Game game;
    private GameMessage message = new GameMessage();

    CommandHandler(Game game) {
        this.game = game;
    }

    public void handle (){

        String command;
        String argument = null;

        Scanner scanner = new Scanner(System. in);
        String input = scanner. nextLine();
        StringTokenizer tokenizer = new StringTokenizer(input);

        // Valid input only 2 words
        if(tokenizer.countTokens() == 1 ){
            command = tokenizer.nextToken();
        }
        else if (tokenizer.countTokens() == 2 ){
            command = tokenizer.nextToken();
            argument = tokenizer.nextToken();
        }else {
            System.out.print("Špatný příkaz!\n\n");
            return;
        }

        if (argument == null) {
            runCommand(command);
        } else {
            runCommand(command, argument);
        }

    }

    //Runs 1 word commands
    private void runCommand(String command){

        if (command.equalsIgnoreCase("mujbatoh")){
            game.player.showInventory();
        } else if (command.equalsIgnoreCase("zpet")){
            if (game.player.getInteractingWithObject() != null){
                game.player.setInteractingWithObject(null);
                message.callMessage("./res/" + game.player.getCurrentLocation().getName() + ".txt");
            } else {
                System.out.print("Špatný příkaz!\n\n");
            }
        }else if (command.equalsIgnoreCase("konec")){
            game.player.setHealth(0);
        }

        else if (command.equalsIgnoreCase("pomoc")){
            System.out.print("Přikaz     Argument          Popis\n" +
                             "Jit        Název mistnosti   Přesune hrače do jiné mistnosti\n" +
                             "Snist      Název věci        Snist věc z batohu\n" +
                             "Vzit       Název věci        Převzit věc\n" +
                             "Podivatse  Název objektu     Interakce s objektem\n" +
                             "Vyuzit     Název věci        Využit věc z batohu\n" +
                             "Konec                        Konec hry\n" +
                             "Pomoc                        Ukáže seznam možných přikazu\n" +
                             "Mujbatoh                     Ukáže obsah batohu\n" +
                             "Zpet                         Vyskočit z interakci s objektem\n");
        }else {
            System.out.print("Špatný příkaz!\n\n");
        }

    }
    //Runs 2 word commands
    private void runCommand(String command, String argument){

        if (command.equalsIgnoreCase("snist")){
            if(game.player.eatFood(argument)){
                System.out.print("Snědl jsi " + argument + " a cítiš se líp\n");
            } else {
                System.out.print("Špatný příkaz!\n\n");
            }
        }else if(command.equalsIgnoreCase("jit") ){
            if ((game.player
                    .getCurrentLocation()
                    .getReachableLocations()
                    .containsKey(argument) &&
                    game.player
                            .getCurrentLocation()
                            .getReachableLocations()
                            .get(argument)
                            .getKey() == null)
                    ||
                    (game.player
                            .getCurrentLocation()
                            .getReachableLocations()
                            .containsKey(argument) &&
                    game.player
                            .getInventory()
                            .getContainer()
                            .containsKey(
                                    game.player
                                            .getCurrentLocation()
                                            .getReachableLocations()
                                            .get(argument)
                                            .getKey()
                                            .getName()
                            )
                    )
                    ){

                game.player.setCurrentLocation(game.locations.get(argument));
                game.player.setInteractingWithObject(null);
                message.callMessage("./res/" + game.player.getCurrentLocation().getName() + ".txt");
                System.out.print("Možné cesty jsou: ");
                Iterator iterator = game.player.getCurrentLocation().getReachableLocations().values().iterator();
                while (iterator.hasNext()){
                    System.out.print(iterator.next().toString());
                    if (iterator.hasNext()){
                        System.out.print(", ");
                    } else {
                        System.out.print("\n");
                    }
                }
                iterator = game.player.getCurrentLocation().getInteractableObjects().values().iterator();
                if (!
                        game.player.getCurrentLocation().getInteractableObjects().isEmpty()) {
                    System.out.print("Zajímave objekty jsou: ");
                    while (iterator.hasNext()) {
                        System.out.print(iterator.next().toString());
                        if (iterator.hasNext()) {
                            System.out.print(", ");
                        } else {
                            System.out.print("\n");
                        }
                    }
                }
                System.out.print("Cítiš se na " + game.player.getHealth() + "% \n");
            } else {
                if ((game.player
                        .getCurrentLocation()
                        .getReachableLocations()
                        .containsKey(argument) &&
                        game.player
                                .getCurrentLocation()
                                .getReachableLocations()
                                .get(argument)
                                .getKey() != null)) {
                    System.out.print("Místnost je uzavřena\n\n");
                } else {
                    System.out.print("Špatný příkaz!\n\n");
                }
            }
        }else if(command.equalsIgnoreCase("podivatse")){
            if (game.player
                    .getCurrentLocation()
                    .getInteractableObjects()
                    .containsKey(argument)
                    ){
                        game.player.setInteractingWithObject(game.player.getCurrentLocation().getInteractableObjects().get(argument));
                        if (game.player.getInteractingWithObject() instanceof RiddleObject){
                            ((RiddleObject) game.player.getCurrentLocation().getInteractableObjects().get(argument)).run(game.player);
                        } else if (game.player.getInteractingWithObject() instanceof ContainerObject) {
                            if (game.player.getInteractingWithObject().getKey() == null ||
                                    game.player
                                            .getInventory()
                                            .getContainer()
                                            .containsValue(game.player.getInteractingWithObject().getKey()) ) {
                                if (game.player.getInteractingWithObject().getKey() != null){
                                    message.callMessage("./res/" + game.player.getInteractingWithObject().getKey().getName() + ".txt");
                                }
                                message.callMessage("./res/" + game.player.getInteractingWithObject().getName() + ".txt");
                                Iterator iterator = ((ContainerObject) game.player
                                        .getCurrentLocation()
                                        .getInteractableObjects()
                                        .get(argument))
                                        .getContainer()
                                        .values()
                                        .iterator();
                                System.out.print("Zajímave objekty jsou: ");
                                while (iterator.hasNext()) {
                                    System.out.print(iterator.next().toString());
                                    if (iterator.hasNext()) {
                                        System.out.print(", ");
                                    } else {
                                        System.out.print("\n");
                                    }
                                }
                            }
                        } else if (game.player.getInteractingWithObject()instanceof StaticObject){
                            if (!((StaticObject) game.player.getInteractingWithObject()).getActivated()){
                                message.callMessage("./res/" + game.player.getInteractingWithObject().getName() + ".txt");
                            } else {
                                message.callMessage("./res/" + game.player.getInteractingWithObject().getName() + "activated.txt");
                            }
                        }
            } else {
                System.out.print("Špatný příkaz!\n\n");
            }
        }else if(command.equalsIgnoreCase("vzit")){
            if (game.player.getInteractingWithObject() != null &&
                    ((ContainerObject) game.player
                            .getInteractingWithObject())
                            .getContainer()
                            .containsKey(argument)
                    ){
                        if(game.player
                                .getInventory()
                                .addItem(((ContainerObject) game.player.getInteractingWithObject()).getContainer().get(argument))){
                            ((ContainerObject) game.player.getInteractingWithObject()).getContainer().remove(argument);
                            System.out.print("Vzal jsi " + argument + "\n");
                        } else {
                            System.out.print("Máš plný batoh\n");
                        }
            } else {
                System.out.print("Špatný příkaz!\n\n");
            }
        }else if(command.equalsIgnoreCase("vyuzit")){
            if (game.player.getInteractingWithObject().getKey() == game.player.getInventory().getContainer().get(argument)){
                message.callMessage("./res/" + argument + ".txt");
                ((StaticObject) game.player.getInteractingWithObject()).setActivated(true);
                game.player.getInventory().removeItem(argument);
            } else {
                System.out.print("Nic se neděje\n");
            }
        }else {
            System.out.print("Špatný příkaz!\n\n");
        }
    }
}
