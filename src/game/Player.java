package game;

import gameObjects.FoodItem;
import gameObjects.InteractableObject;

public class Player {

    private Location currentLocation = null;
    private int health = 100;
    private Inventory inventory = new Inventory(10);
    private boolean success = false;
    private InteractableObject interactingWithObject = null;

    public void setInteractingWithObject(InteractableObject interactingWithObject) {
        this.interactingWithObject = interactingWithObject;
    }

    public InteractableObject getInteractingWithObject() {
        return interactingWithObject;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public Location getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(Location currentLocation) {
        this.currentLocation = currentLocation;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    //Restores players health and removes food item if player has food item in inventory
    public boolean eatFood (String name) {
        if (inventory.getContainer().containsKey(name)) {
                health += ((FoodItem) inventory.getContainer().get(name)).getRestore();
            inventory.removeItem(name);
            return true;
        }
        return false;

    }
    //Shows all objects in players inventory
    public void showInventory (){
        if (inventory.getContainer().isEmpty()){
            System.out.print("Batoh je prázdný\n");
        } else {
            for (String key : inventory.getContainer().keySet()){
                System.out.print(key + "\n");
            }
        }
    }



}
