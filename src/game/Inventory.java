package game;

import gameObjects.CollectableItem;

import java.util.HashMap;

public class Inventory {

    private int size;
    private HashMap<String, CollectableItem> container = new HashMap<>();
    public Inventory(int size) {
        this.size = size;
    }

    public HashMap<String, CollectableItem> getContainer() {
        return container;
    }

    //Adds collactable item to collection depends on previous defined size and items identity
    public boolean addItem(CollectableItem collectableItem){
         if (!container.containsValue(collectableItem )&& container.size() < size ){
             container.put(collectableItem.getName(),collectableItem);
             return true;
         }
         return false;
    }
    //Removes item from collection, searching by name
    public void removeItem(String  item){
        if (container.containsKey( item)){
            container.remove(item);
        }
    }

}
