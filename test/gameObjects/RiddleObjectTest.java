package gameObjects;

import game.Player;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class RiddleObjectTest {

    Player player = new Player();
    RiddleObject riddleObject = new RiddleObject("test","test");

    @Test
    public void run() {

        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(byteArrayOutputStream);
        ByteArrayInputStream in = new ByteArrayInputStream("test".getBytes());
        System.setOut(ps);
        System.setIn(in);
        riddleObject.run(player);
        in = new ByteArrayInputStream("testbad".getBytes());
        System.setIn(in);
        riddleObject.run(player);
        Assert.assertEquals(70,player.getHealth());
        Assert.assertEquals("test\r\ntestgood\r\ntest\r\ntestbad\r\nCítiš se na 70% \n",byteArrayOutputStream.toString());
    }

    @Test
    public void testToString() {
        Assert.assertEquals("test",riddleObject.toString());
    }

    @Test
    public void getName() {
        Assert.assertEquals("test",riddleObject.getName());
    }

    @Test
    public void getKey() {
        Assert.assertEquals(null,riddleObject.getKey());
    }
}