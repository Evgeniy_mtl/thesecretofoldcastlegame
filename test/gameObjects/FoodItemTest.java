package gameObjects;

import org.junit.Assert;
import org.junit.Test;

public class FoodItemTest {

    FoodItem foodItem = new FoodItem( "test", 30);

    @Test
    public void getName() {
        Assert.assertEquals("test",foodItem.getName());
    }

    @Test
    public void testToString() {
        Assert.assertEquals("test",foodItem.toString());
    }

    @Test
    public void getRestore() {
        Assert.assertEquals(30,foodItem.getRestore());
    }
}