package gameObjects;

import org.junit.Assert;
import org.junit.Test;

public class KeyItemTest {

    KeyItem keyItem = new KeyItem("test");

    @Test
    public void getName() {
        Assert.assertEquals("test",keyItem.getName());
    }

    @Test
    public void testToString() {
        Assert.assertEquals("test",keyItem.toString());
    }
}