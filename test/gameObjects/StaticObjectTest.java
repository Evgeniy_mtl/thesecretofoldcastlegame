package gameObjects;

import org.junit.Assert;
import org.junit.Test;

public class StaticObjectTest {

    KeyItem keyItem = new KeyItem("test");
    StaticObject staticObject = new StaticObject("test", keyItem);
    @Test
    public void getActivated() {
        Assert.assertEquals(false,staticObject.getActivated());
    }

    @Test
    public void setActivated() {
        staticObject.setActivated(true);
        Assert.assertEquals(true,staticObject.getActivated());
    }

    @Test
    public void testToString() {
        Assert.assertEquals("test",staticObject.toString());
    }

    @Test
    public void getName() {
        Assert.assertEquals("test",staticObject.getName());
    }

    @Test
    public void getKey() {
        Assert.assertEquals(keyItem,staticObject.getKey());
    }
}