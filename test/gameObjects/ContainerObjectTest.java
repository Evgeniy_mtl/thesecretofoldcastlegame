package gameObjects;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;

public class ContainerObjectTest {

    ContainerObject containerObject = new ContainerObject("test");
    KeyItem keyItem = new KeyItem("testkey");
    KeyItem test = new KeyItem("test");
    ContainerObject containerObjectlocked = new ContainerObject("testlocked", keyItem );
    private HashMap<String, CollectableItem> container = new HashMap<>();


    @Test
    public void testToString() {
        Assert.assertEquals("test",containerObject.toString());
        Assert.assertEquals("testlocked",containerObjectlocked.toString());
    }

    @Test
    public void getKey() {
        Assert.assertEquals(keyItem,containerObjectlocked.getKey());
    }

    @Test
    public void getContainer() {
        Assert.assertEquals(container,containerObject.getContainer());
        container.put("test",test);
        containerObject.getContainer().put("test",test);
        Assert.assertEquals(container,containerObject.getContainer());
    }

    @Test
    public void getName() {
        Assert.assertEquals("test", containerObject.getName());

    }
}