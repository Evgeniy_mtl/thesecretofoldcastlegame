package game;

import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class GameMessageTest {

    GameMessage message = new GameMessage();

    @Test
    public void callMessage() {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(byteArrayOutputStream);
        System.setOut(ps);
        message.callMessage("test");
        message.callMessage("./res/test.txt");
        Assert.assertEquals("Soubor nenalezen\n\r\ntest\r\n",byteArrayOutputStream.toString());
    }
}