package game;

import gameObjects.FoodItem;
import gameObjects.KeyItem;
import gameObjects.StaticObject;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class PlayerTest {
    Player player = new Player();
    Location location = new Location("test");
    StaticObject staticObject = new StaticObject("test");
    KeyItem keyItem = new KeyItem("test");
    FoodItem foodItem = new FoodItem("test",30);
    Inventory inventory = new Inventory(10);

    @Test
    public void setInteractingWithObject() {
        Assert.assertEquals(null,player.getInteractingWithObject());
        player.setInteractingWithObject(staticObject);
        Assert.assertEquals(staticObject,player.getInteractingWithObject());
    }

    @Test
    public void getInteractingWithObject() {
        Assert.assertEquals(null,player.getInteractingWithObject());
        player.setInteractingWithObject(staticObject);
        Assert.assertEquals(staticObject,player.getInteractingWithObject());
    }

    @Test
    public void setSuccess() {
        Assert.assertEquals(false,player.isSuccess());
        player.setSuccess(true);
        Assert.assertEquals(true,player.isSuccess());
    }

    @Test
    public void isSuccess() {
        Assert.assertEquals(false,player.isSuccess());
        player.setSuccess(true);
        Assert.assertEquals(true,player.isSuccess());
    }

    @Test
    public void getInventory() {
        Assert.assertEquals(inventory.getContainer(),player.getInventory().getContainer());
        player.getInventory().addItem(keyItem);
        inventory.addItem(keyItem);
        Assert.assertEquals(inventory.getContainer(),player.getInventory().getContainer());
    }

    @Test
    public void getCurrentLocation() {
        Assert.assertEquals(null,player.getCurrentLocation());
        player.setCurrentLocation(location);
        Assert.assertEquals(location,player.getCurrentLocation());
    }

    @Test
    public void setCurrentLocation() {
        Assert.assertEquals(null,player.getCurrentLocation());
        player.setCurrentLocation(location);
        Assert.assertEquals(location,player.getCurrentLocation());
        player.setCurrentLocation(null);
        Assert.assertEquals(null,player.getCurrentLocation());
    }

    @Test
    public void getHealth() {
        Assert.assertEquals(100,player.getHealth());
        player.setHealth(70);
        Assert.assertEquals(70,player.getHealth());
    }

    @Test
    public void setHealth() {
        Assert.assertEquals(100,player.getHealth());
        player.setHealth(70);
        Assert.assertEquals(70,player.getHealth());
    }

    @Test
    public void eatFood() {
        Assert.assertEquals(false,player.eatFood("test"));
        player.getInventory().addItem(foodItem);
        Assert.assertEquals(true,player.eatFood("test"));
    }

    @Test
    public void showInventory() {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(byteArrayOutputStream);
        System.setOut(ps);
        player.showInventory();
        Assert.assertEquals("Batoh je prázdný\n",byteArrayOutputStream.toString());
        player.getInventory().addItem(keyItem);
        player.showInventory();
        Assert.assertEquals("Batoh je prázdný\ntest\n",byteArrayOutputStream.toString());
    }
}