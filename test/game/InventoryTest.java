package game;

import gameObjects.CollectableItem;
import gameObjects.KeyItem;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;

public class InventoryTest {

    KeyItem keyItem = new KeyItem("test");
    KeyItem keyItem2 = new KeyItem("test2");
    KeyItem keyItem3 = new KeyItem("test3");
    Inventory inventory = new Inventory(2);
    private HashMap<String, CollectableItem> container = new HashMap<>();

    @Test
    public void getContainer() {
        Assert.assertEquals(container,inventory.getContainer());
    }

    @Test
    public void addItem() {
        container.put("test",keyItem);
        Assert.assertEquals(true, inventory.addItem(keyItem));
        Assert.assertEquals(container,inventory.getContainer());
        Assert.assertEquals(false, inventory.addItem(keyItem));
        Assert.assertEquals(true, inventory.addItem(keyItem2));
        Assert.assertEquals(false, inventory.addItem(keyItem3));
    }

    @Test
    public void removeItem() {
        inventory.addItem(keyItem);
        Assert.assertNotEquals(container,inventory.getContainer());
        inventory.removeItem("test");
        Assert.assertEquals(container,inventory.getContainer());
    }

}