package game;

import gameObjects.ContainerObject;
import gameObjects.FoodItem;
import gameObjects.KeyItem;
import gameObjects.StaticObject;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class CommandHandlerTest {
    Game game = new Game();
    CommandHandler commandHandler = new CommandHandler(game);

    Location location = new Location("test");
    Location location1 = new Location("test1");
    KeyItem keyItem = new KeyItem("test");
    KeyItem keyItem1 = new KeyItem("test1");
    StaticObject staticObjectlocked = new StaticObject("test",keyItem);
    StaticObject staticObject = new StaticObject("test");
    ContainerObject containerObject = new ContainerObject("test");
    FoodItem foodItem = new FoodItem("test",30);

    @Test
    public void handle() {
        game.player.getInventory().addItem(foodItem);
        containerObject.getContainer().put(keyItem1.getName(),keyItem1);
        location.getReachableLocations().put(location1.getName(), location1);
        location1.getReachableLocations().put(location.getName(), location);
        location.getInteractableObjects().put(staticObject.getName(), staticObject);
        game.locations.put(location.getName(),location);
        game.locations.put(location1.getName(),location1);
        game.player.setCurrentLocation(location);
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(byteArrayOutputStream);
        ByteArrayInputStream in = new ByteArrayInputStream("snist test".getBytes());
        System.setOut(ps);
        System.setIn(in);
        commandHandler.handle();
        Assert.assertEquals("Snědl jsi test a cítiš se líp\n",byteArrayOutputStream.toString());
        byteArrayOutputStream.reset();
        in = new ByteArrayInputStream("snist snist".getBytes());
        System.setIn(in);
        commandHandler.handle();
        Assert.assertEquals("Špatný příkaz!\n\n",byteArrayOutputStream.toString());
        byteArrayOutputStream.reset();
        in = new ByteArrayInputStream("konec".getBytes());
        System.setIn(in);
        commandHandler.handle();
        Assert.assertEquals("",byteArrayOutputStream.toString());
        byteArrayOutputStream.reset();
        in = new ByteArrayInputStream("test".getBytes());
        System.setIn(in);
        commandHandler.handle();
        Assert.assertEquals("Špatný příkaz!\n\n",byteArrayOutputStream.toString());
        byteArrayOutputStream.reset();
        in = new ByteArrayInputStream("jit".getBytes());
        System.setIn(in);
        commandHandler.handle();
        Assert.assertEquals("Špatný příkaz!\n\n",byteArrayOutputStream.toString());
        byteArrayOutputStream.reset();
        in = new ByteArrayInputStream("pomoc".getBytes());
        System.setIn(in);
        commandHandler.handle();
        Assert.assertEquals("Přikaz     Argument          Popis\n" +
                "Jit        Název mistnosti   Přesune hrače do jiné mistnosti\n" +
                "Snist      Název věci        Snist věc z batohu\n" +
                "Vzit       Název věci        Převzit věc\n" +
                "Podivatse  Název objektu     Interakce s objektem\n" +
                "Vyuzit     Název věci        Využit věc z batohu\n" +
                "Konec                        Konec hry\n" +
                "Pomoc                        Ukáže seznam možných přikazu\n" +
                "Mujbatoh                     Ukáže obsah batohu\n" +
                "Zpet                         Vyskočit z interakci s objektem\n",byteArrayOutputStream.toString());
        byteArrayOutputStream.reset();
        game.player.getInventory().addItem(keyItem);
        in = new ByteArrayInputStream("mujbatoh".getBytes());
        System.setIn(in);
        commandHandler.handle();
        Assert.assertEquals("test\n",byteArrayOutputStream.toString());
        byteArrayOutputStream.reset();
        Assert.assertEquals(null,game.player.getInteractingWithObject());
        game.player.setInteractingWithObject(staticObject);
        Assert.assertEquals(staticObject,game.player.getInteractingWithObject());
        in = new ByteArrayInputStream("zpet".getBytes());
        System.setIn(in);
        commandHandler.handle();
        Assert.assertEquals(null,game.player.getInteractingWithObject());
        Assert.assertEquals("test\r\n",byteArrayOutputStream.toString());
        byteArrayOutputStream.reset();
        Assert.assertEquals(null,game.player.getInteractingWithObject());
        in = new ByteArrayInputStream("podivatse test".getBytes());
        System.setIn(in);
        commandHandler.handle();
        Assert.assertEquals(staticObject,game.player.getInteractingWithObject());
        Assert.assertEquals("test\r\n",byteArrayOutputStream.toString());
        byteArrayOutputStream.reset();
        game.player.setInteractingWithObject(containerObject);
        in = new ByteArrayInputStream("vzit test1".getBytes());
        System.setIn(in);
        commandHandler.handle();
        Assert.assertEquals("Vzal jsi test1\n",byteArrayOutputStream.toString());
        byteArrayOutputStream.reset();
        in = new ByteArrayInputStream("jit test1".getBytes());
        System.setIn(in);
        commandHandler.handle();

        /*Assert.assertEquals("test1\r\n" +
                "Možné cesty jsou: test\r\n" +
                "Cítiš se na 0% \r\n",byteArrayOutputStream.toString());
        */
        Assert.assertEquals(location1,game.player.getCurrentLocation());
        byteArrayOutputStream.reset();
        game.player.setInteractingWithObject(staticObjectlocked);
        in = new ByteArrayInputStream("vyuzit test".getBytes());
        System.setIn(in);
        commandHandler.handle();
        Assert.assertEquals("test\r\n",byteArrayOutputStream.toString());

    }
}