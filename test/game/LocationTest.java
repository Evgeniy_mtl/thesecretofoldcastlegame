package game;

import gameObjects.InteractableObject;
import gameObjects.KeyItem;
import gameObjects.StaticObject;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;

public class LocationTest {
    KeyItem keyItem = new KeyItem("testKey");
    StaticObject staticObject = new StaticObject("test");
    Location location = new Location("test",keyItem);
    Location location1 = new Location("test1");
    private HashMap<String, InteractableObject> interactableObjects = new HashMap<>();
    private HashMap <String, Location> reachableLocations = new HashMap<>();

    @Test
    public void getName() {
        Assert.assertEquals("test",location.getName());
    }

    @Test
    public void getKey() {
        Assert.assertEquals(keyItem,location.getKey());
    }

    @Test
    public void getInteractableObjects() {
        interactableObjects.put(staticObject.getName(),staticObject);
        location.getInteractableObjects().put(staticObject.getName(),staticObject);
        Assert.assertEquals(interactableObjects, location.getInteractableObjects());
    }

    @Test
    public void getReachableLocations() {
        location.getReachableLocations().put(location1.getName(),location1);
        reachableLocations.put(location1.getName(),location1);
        Assert.assertEquals(reachableLocations, location.getReachableLocations());
    }

    @Test
    public void testToString() {
        Assert.assertEquals("test",location.toString());
    }

}