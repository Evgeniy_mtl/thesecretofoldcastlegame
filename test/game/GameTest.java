package game;

import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class GameTest {
    Game game = new Game();
    Game game1 = new Game();

    @Test
    public void startGame() {
        game.initGame();

        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(byteArrayOutputStream);
        ByteArrayInputStream in = new ByteArrayInputStream("konec".getBytes());
        System.setOut(ps);
        System.setIn(in);
        game.startGame();
        Assert.assertEquals("Před vámi se otevírají dveře starého hradu a vstupujete do hlavního sálu.\r\n" +
                "Hra je ukončena, nezvladl jsi to.",byteArrayOutputStream.toString());
    }

    @Test
    public void initGame() {
        game.initGame();
        game1.initGame();
        Assert.assertEquals(game.locations.keySet(),game1.locations.keySet());
        Assert.assertEquals(game.player.getCurrentLocation().getName(),game1.player.getCurrentLocation().getName());
        Assert.assertEquals(game.player.getInteractingWithObject(),game1.player.getInteractingWithObject());
        Assert.assertEquals(game.player.getInventory().getContainer().keySet(),game1.player.getInventory().getContainer().keySet());
        Assert.assertEquals(game.player.getHealth(),game1.player.getHealth());
        Assert.assertEquals(game.player.isSuccess(),game1.player.isSuccess());
    }
}